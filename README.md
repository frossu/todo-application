# To do application

## Running on your own machine from CLI
1. Make sure dotnet cli is installed: `dotnet --version`
2. Get a PostgreSQL database connection string and put it in `./todo-aspnetmvc-ui/appsettings.json`
3. Run `dotnet run --project ./todo-aspnetmvc-ui`

There is also a demo available, try it out: [fly.io deployment](https://muddy-cherry-6634.fly.dev/)

