using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace todo_domain_entities
{
    /// <summary>
    /// An entity representing a list of tasks.
    /// </summary>
    public class TaskList : BaseEntity
    {
        /// <summary>
        /// Gets or sets a userName of the list.
        /// </summary>
        /// <value>Username of the user to which the list belongs.</value>
        public string UserId { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }

        /// <summary>
        /// Gets or sets a name of the list.
        /// </summary>
        /// <value>name of the list.</value>
        public string Name { get; set; } = "Untitled";

        /// <summary>
        /// Gets or sets a value indicating whether the list is hidden in the list view.
        /// </summary>
        /// <value>true or false.</value>
        public bool IsHidden { get; set; }

        /// <summary>
        /// Gets a list of tasks.
        /// </summary>
        /// <value>tasks list.</value>
        public virtual List<TaskEntry> Tasks { get; set;  }
    }
}
