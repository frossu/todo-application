using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities
{
    public enum TaskStatus
    {
        [Display(Name = "Completed")]
        TaskCompleted,
        [Display(Name = "In progress")]
        TaskInProgress,
        [Display(Name = "Not started")]
        TaskNotStarted
    }
}