using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using todo_domain_entities;

namespace todo_domain_entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        [JsonIgnore]
        public virtual List<TaskList> TaskLists { get; set; }
    }
}