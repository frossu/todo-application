using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace todo_domain_entities
{
    /// <summary>
    /// An entity representing a task.
    /// </summary>
    public class TaskEntry : BaseEntity
    {
        /// <summary>
        /// Gets or sets an identifier of the list to which the task belongs.
        /// </summary>
        /// <value>Id of a list.</value>
        public int TaskListId { get; set; }

        [JsonIgnore]
        public virtual TaskList TaskList { get; set; }

        /// <summary>
        /// Gets or sets a name of the task.
        /// </summary>
        /// <value><see cref="string" /> name of the task.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a description of the task.
        /// </summary>
        /// <value>string[500 chars max].</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets date and time by which the task is due.
        /// </summary>
        /// <value>Date by which a task the task is due.</value>
        [DataType(DataType.Date)]
        public DateTime? DueDate { get; set; }
        
        /// <summary>
        /// Gets a date and time when the task was created.
        /// </summary>
        /// <value><see cref="System.DateTime"/> when the task was created.</value>
        public DateTime CreatedAt { get; set;  }

        /// <summary>
        /// Gets or sets a value indicating whether the task is completed.
        /// </summary>
        /// <value>true or false.</value>
        public bool IsCompleted => Status == TaskStatus.TaskNotStarted;

        /// <summary>
        /// Gets or sets a status of a task.
        /// </summary>
        /// <value>instance of TaskStatus.</value>
        public TaskStatus Status { get; set; } = TaskStatus.TaskNotStarted;
    }
}
