using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using todo_aspnetmvc_ui.Models.Todo;
using todo_aspnetmvc_ui.Services;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Controllers
{
    [Authorize]
    [Route("Todo")]
    public class TodoController : Controller
    {
        private readonly ITodoListService _service;
        private readonly UserManager<ApplicationUser> _userManager;

        public TodoController(ITodoListService service, UserManager<ApplicationUser> userManager)
        {
            _service = service;
            _userManager = userManager;
        }

        [HttpGet("{id?}")]
        public IActionResult Index(int? id)
        {
            if (id == null) return RedirectToAction("Index", new {id = _service.GetFirstListIdForUser()});

            var currentList = _service.GetTaskListById(id.Value);
            if (currentList.UserId != _userManager.GetUserId(User))
            {
                return BadRequest($"The list with id {currentList.Id} doesn't belong to the current user.");
            }
            return View(currentList);
        }

        [HttpGet("CreateTask")]
        public IActionResult CreateTask([FromQuery] int listId)
        {

            ViewBag.ListId = listId;
            var list = _service.GetListById(listId);
            if (list == null)
            {
                return NotFound();
            }
            else if (list.UserId != _userManager.GetUserId(User)) {
                return BadRequest($"The list with id {list.Id} doesn't belong to the current user.");
            }

            return View(new CreateTaskViewModel());
        }

        [HttpPost("CreateTask")]
        public IActionResult CreateTask(CreateTaskViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            #region not-so-good-code

            var task = new TaskEntry
            {
                TaskListId = viewModel.TaskListId,
                Name = viewModel.Name,
                Description = viewModel.Description,
                DueDate = viewModel.DueDate
            };

            #endregion

            var taskList = _service.GetListById(viewModel.TaskListId);
            if (taskList.UserId != _userManager.GetUserId(User)) {
                return BadRequest($"The list with id {taskList.Id} doesn't belong to the current user.");
            }

            _service.AddTask(task.TaskListId, task);

            return RedirectToAction("Index", new {id = task.TaskListId});
        }

        [HttpPost]
        public IActionResult CreateList()
        {
            var taskList = _service.AddList(new TaskList());
            return RedirectToAction("Index", new {taskList.Id});
        }

        [HttpPost("{id}")]
        public IActionResult DeleteList(int id)
        {
            var taskList = _service.GetListById(id);
            if (taskList == null)
            {
                return NotFound();
            }

            if (taskList.UserId != _userManager.GetUserId(User))
            {
                return BadRequest($"The list with id {id} doesn't belong to the current user.");
            }

            _service.DeleteList(id);

            return RedirectToAction("Index");
        }

        // [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        // public IActionResult Error()
        // {
        // return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        // }
        [HttpGet("EditTask/{id}")]
        public IActionResult EditTask(int id)
        {
            var taskEntry = _service.GetTaskById(id);
            if (taskEntry == null)
            {
                return NotFound();
            }

            return View(new EditTaskViewModel
            {
                TaskId = id,
                TaskStatus = taskEntry.Status,
                DueDate = taskEntry.DueDate,
                Description = taskEntry.Description,
                Name = taskEntry.Name,
                TaskListId = taskEntry.TaskListId
            });
        }

        [HttpPost("EditTask/{id}")]
        public IActionResult EditTask(int id, EditTaskViewModel viewModel)
        {
            // return Ok("kek");

            if (!ModelState.IsValid)
            {
                return View();
            }

            if (viewModel.TaskId != id)
            {
                return BadRequest("Ids don't match");
            }

            var task = _service.GetTaskById(id);

            task.Name = viewModel.Name;
            task.Description = viewModel.Description;
            task.DueDate = viewModel.DueDate;
            task.Status = viewModel.TaskStatus;
            
            _service.UpdateTask(task.Id, task);
            return RedirectToAction("Index", new {viewModel.TaskListId});
        }

        [HttpGet("EditList")]
        public IActionResult EditList(int id)
        {
            var list = _service.GetListById(id);
            if (list == null)
            {
                return NotFound();
            }
            
            return Ok("norm");
        }
    }
}