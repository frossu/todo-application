using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using todo_aspnetmvc_ui.Models.Account;
using todo_aspnetmvc_ui.Services;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Controllers
{
    // [Route("[controller]")]
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signinManager;
        private readonly ITodoListService _service;

        public AccountController(ILogger<AccountController> logger, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signinManager, ITodoListService service)
        {
            _logger = logger;
            _userManager = userManager;
            _signinManager = signinManager;
            _service = service;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registration)
        {
            if (!ModelState.IsValid)
            {
                return View(registration);
            }

            _logger.LogInformation("Registering a user with Name {Name}<{EmailAddress}>", registration.Name,
                registration.EmailAddress);
            var newUser = new ApplicationUser
            {
                Email = registration.EmailAddress,
                UserName = registration.EmailAddress,
                Name = registration.Name
            };

            var result = await _userManager.CreateAsync(newUser, registration.Password);


            if (result.Succeeded)
            {
                _service.CreateDefaultListForUser(newUser.Id);

                return RedirectToAction("Login");
            }

            foreach (var error in result.Errors.Select(x => x.Description))
            {
                ModelState.AddModelError("", error);
            }

            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel login, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var result =
                await _signinManager.PasswordSignInAsync(login.EmailAddress, login.Password, login.RememberMe, false);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Login error!");
                return View();
            }

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction("Index", "Home");
            }

            return Redirect(returnUrl);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Logout(string returnUrl = null)
        {
            await _signinManager.SignOutAsync();

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction("Index", "Home");
            }

            return Redirect(returnUrl);
        }
    }
}