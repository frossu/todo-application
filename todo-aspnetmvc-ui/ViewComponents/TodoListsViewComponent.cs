using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using todo_aspnetmvc_ui.Services;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.ViewComponents
{
    public class TodoListsViewComponent : ViewComponent
    {
        private readonly ITodoListService service;
        private readonly ILogger<TodoListsViewComponent> logger;

        public TodoListsViewComponent(ILogger<TodoListsViewComponent> logger, ITodoListService service)
        {
            this.service = service;
            this.logger = logger;
        }

        public IViewComponentResult Invoke()
        {
            var todoLists = service.GetListsForUser().ToArray();
            // logger.LogInformation("Found items {0}", JsonSerializer.Serialize(todoLists));
            return View(todoLists);
        }
    }
}