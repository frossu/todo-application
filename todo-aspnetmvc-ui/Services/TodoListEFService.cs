﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using todo_aspnetmvc_ui.Models;
using todo_domain_entities;
using TaskStatus = todo_domain_entities.TaskStatus;

namespace todo_aspnetmvc_ui.Services
{
    /// <summary>
    /// Utility class for the ToDoList application.
    /// </summary>
    public sealed class TodoListEfService : ITodoListService
    {
        private readonly TodoDbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<TodoListEfService> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListUtils"/> class.
        /// with the given database context.
        /// </summary>
        /// <param name="dbContext">database context.</param>
        public TodoListEfService(TodoDbContext dbContext, IHttpContextAccessor httpContextAccessor,
            UserManager<ApplicationUser> userManager, ILogger<TodoListEfService> logger)
        {
            this._dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _logger = logger;
        }

        /// <inheritdoc />
        public TaskList AddList(TaskList taskList)
        {
            if (taskList == null)
            {
                throw new ArgumentNullException(nameof(taskList));
            }
            else if (taskList.Name == null)
            {
                throw new ArgumentException("The list name cannot be null", nameof(taskList));
            }
            else if (taskList.Name.Length == 0)
            {
                throw new ArgumentException("The list name cannot be empty.", nameof(taskList));
            }

            taskList.UserId = _userManager.GetUserId(_httpContextAccessor.HttpContext.User);

            var created = this._dbContext.TaskLists.Add(taskList);
            this._dbContext.SaveChanges();
            return created.Entity;
        }

        /// <inheritdoc />
        public IEnumerable<TaskList> GetListsForUser()
        {
            return GetListsForUser(_userManager.GetUserId(_httpContextAccessor.HttpContext.User));
        }

        public TaskList GetListById(int id)
        {
            return _dbContext.TaskLists.FirstOrDefault(l => l.Id == id);
        }

        /// <inheritdoc />
        public IEnumerable<TaskList> GetListsForUser(string userId)
        {
            return this._dbContext.TaskLists.Where(l => l.UserId == userId).ToList();
        }

        /// <inheritdoc />
        public IEnumerable<TaskEntry> GetListTasks(int listId)
        {
            var taskList = this.GetListOrThrow(listId);
            var tasks = taskList.Tasks;
            return tasks.ToList();
        }

        /// <inheritdoc />
        public void EditList(int id, string name)
        {
            var taskList = this.GetListOrThrow(id);
            taskList.Name = name;
            this._dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteList(int id)
        {
            var taskList = this.GetListOrThrow(id);
            this._dbContext.TaskLists.Remove(taskList);
            this._dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public void AddTask(int listId, TaskEntry task)
        {
            var taskList = this.GetListOrThrow(listId);
            task.CreatedAt = DateTime.Now;
            taskList.Tasks.Add(task);
            this._dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public TaskEntry DeleteTask(int taskId)
        {
            var task = this._dbContext.TaskEntries.Find(taskId);
            this._dbContext.TaskEntries.Remove(task);
            this._dbContext.SaveChanges();
            return task;
        }

        /// <inheritdoc />
        public void AssignTaskToList(int taskId, int newListId)
        {
            var task = this._dbContext.TaskEntries.Find(taskId);
            var newList = this.GetListOrThrow(newListId);
            var oldList = this._dbContext.TaskLists.Find(task.TaskListId);
            oldList.Tasks.Remove(task);
            newList.Tasks.Add(task);
            this._dbContext.SaveChanges();
        }

        public TaskList GetTaskListById(int id)
        {
            var taskList = this._dbContext.TaskLists
                .Include(l => l.Tasks)
                .FirstOrDefault(l => l.Id == id);
            return taskList;
        }

        public void CreateDefaultListForUser(string userId)
        {
            _logger.LogInformation("Creating default list for user {NewUserId}", userId);
            var taskList = new TaskList {Name = "Getting Started", UserId = userId};
            var taskEntry2 = new TaskEntry {TaskListId = taskList.Id, Name = "Bar", Description = "Baz", CreatedAt = DateTime.Now};
            var taskEntry = new TaskEntry {TaskListId = taskList.Id, Name = "Foo", Description = "Bar", CreatedAt = DateTime.Now};
            _logger.LogInformation("taskEntry {Id}", taskEntry.Id);
            _logger.LogInformation("taskEntry2 {Id}", taskEntry2.Id);

            taskList.Tasks = new List<TaskEntry>()
            {
                taskEntry,
                taskEntry2
            };
            this._dbContext.Add(taskList);
            this._dbContext.SaveChanges();
        }

        public int? GetFirstListIdForUser()
        {
            var userId = _userManager.GetUserId(_httpContextAccessor.HttpContext.User);
            return _dbContext.TaskLists.Where(l => l.UserId == userId)
                .Select(l => l.Id).OrderBy(id => id).FirstOrDefault();
        }

        public TaskEntry GetTaskById(int id)
        {
            return _dbContext.TaskEntries.FirstOrDefault(t => t.Id == id);
        }

        /// <inheritdoc />
        public void UpdateTask(int taskId, TaskEntry task)
        {
            this._dbContext.Entry(task).State = EntityState.Modified;
            this._dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public void HideList(int id)
        {
            var TaskList = this.GetListOrThrow(id);
            TaskList.IsHidden = true;
            this._dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public void UnhideList(int id)
        {
            var TaskList = this.GetListOrThrow(id);
            TaskList.IsHidden = false;
            this._dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public void UpdateTaskStatus(int taskId, TaskStatus status)
        {
            var task = this._dbContext.TaskEntries.Find(taskId);
            task.Status = status;
            this._dbContext.SaveChanges();
        }

        private TaskList GetListOrThrow(int id)
        {
            var list = this._dbContext.TaskLists.Include(l => l.Tasks).FirstOrDefault(l => l.Id == id);
            if (list == null)
            {
                throw new ArgumentException($"List with id {id} does not exist.");
            }

            return list;
        }
    }
}