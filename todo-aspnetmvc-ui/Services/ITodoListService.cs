using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using todo_domain_entities;
using TaskStatus = todo_domain_entities.TaskStatus;

namespace todo_aspnetmvc_ui.Services
{
    /// <summary>
    /// CLI client for the ToDoList application.
    /// </summary>
    public interface ITodoListService
    {
        TaskList GetListById(int id);
        /// <summary>
        /// Prints the list of lists.
        /// </summary>
        /// <param name="userId">Id of the user.</param>
        /// <returns>a list of todo lists.</returns>
        IEnumerable<TaskList> GetListsForUser(string userId);

        /// <summary>
        /// Prints the list of lists.
        /// </summary>
        /// <returns>a list of todo lists.</returns>
        IEnumerable<TaskList> GetListsForUser();

        /// <summary>
        /// Creates a new list.
        /// </summary>
        /// <param name="name">name of the list.</param>
        TaskList AddList(TaskList taskList);

        /// <summary>
        /// Edits an existing list's name.
        /// </summary>
        /// <param name="id">list identifier.</param>
        /// <param name="name">new name of the list.</param>
        void EditList(int id, string name);

        /// <summary>
        /// Hides a list from list view.
        /// </summary>
        /// <param name="listId">list identifier.</param>
        void HideList(int listId);

        /// <summary>
        /// Brings a list back to list view.
        /// </summary>
        /// <param name="listId">list identifier.</param>
        void UnhideList(int listId);

        /// <summary>
        /// Deletes a list.
        /// </summary>
        /// <param name="listId">list identifier.</param>
        void DeleteList(int listId);

        /// <summary>
        /// Prints the list of tasks.
        /// </summary>
        /// <param name="listId">list identifier.</param>
        /// <returns>a list of tasks in a specified list.</returns>
        IEnumerable<TaskEntry> GetListTasks(int listId);

        /// <summary>
        /// Creates a new task.
        /// </summary>
        /// <param name="listId">list identifier.</param>
        /// <param name="task">a task to add.</param>
        void AddTask(int listId, TaskEntry task);

        /// <summary>
        /// Edits an existing task's name.
        /// </summary>
        /// <param name="taskId">id of a task to update.</param>
        /// <param name="task">updated task data.</param>
        void UpdateTask(int taskId, TaskEntry task);

        void UpdateTaskStatus(int taskId, TaskStatus status);

        /// <summary>
        /// Deletes a task.
        /// </summary>
        /// <param name="taskId">task identifier.</param>
        TaskEntry DeleteTask(int taskId);

        /// <summary>
        /// Assigns a task to a new list.
        /// </summary>
        /// <param name="taskId">task identifier.</param>
        /// <param name="newListId">new list identifier.</param>
        void AssignTaskToList(int taskId, int newListId);

        TaskList GetTaskListById(int id);
        void CreateDefaultListForUser(string newUserId);
        int? GetFirstListIdForUser();
        TaskEntry GetTaskById(int id);
    }
}