using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models
{
    /// <summary>
    /// Database context for the tasks list.
    /// </summary>
    public class TodoDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        /// <summary>
        ///  Initializes a new instance of the <see cref="TasksDbContext"/> class.
        /// </summary>
        public TodoDbContext(DbContextOptions<TodoDbContext> options) : base(options)
        {
            this.Database.Migrate();
        }

        /// <summary>
        /// Gets or Sets task lists db set.
        /// </summary>
        /// <value>task lists.</value>
        public DbSet<TaskList> TaskLists { get; set; }

        /// <summary>
        /// Gets or Sets task entries db set.
        /// </summary>
        /// <value>task entries.</value>
        public DbSet<TaskEntry> TaskEntries { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<ApplicationUser>()
                .HasMany(u => u.TaskLists)
                .WithOne(l => l.User)
                .HasForeignKey(l => l.UserId);

            modelBuilder
                .Entity<TaskList>()
                .HasMany(l => l.Tasks)
                .WithOne(t => t.TaskList)
                .HasForeignKey(t => t.TaskListId);

            modelBuilder
                .Entity<TaskEntry>()
                .HasOne(t => t.TaskList);

            modelBuilder
                .Entity<TaskList>()
                .HasOne(l => l.User);
            
            modelBuilder.Seed();
        }
    }
}
