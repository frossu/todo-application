using System;
using System.ComponentModel.DataAnnotations;

namespace todo_aspnetmvc_ui.Models.Todo
{
    public class CreateTaskViewModel
    {
        [Required] public int TaskListId { get; set; }

        [Display(Name = "Title")]
        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        [Display(Name = "Due")]
        public DateTime? DueDate { get; set; }
    }
}