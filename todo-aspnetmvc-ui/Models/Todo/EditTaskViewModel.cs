using System;
using System.ComponentModel.DataAnnotations;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.Todo
{
    public class EditTaskViewModel
    {
        [Required]
        public int TaskId { get; set; }
        [Required]
        public int TaskListId { get; set; }

        [Display(Name = "Title")]
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Display(Name = "Due")]
        public DateTime? DueDate { get; set; }

        [Required]
        public TaskStatus TaskStatus { get; set; }
    }
}